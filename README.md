# Arduino Integration Demo

> This short project show a simple proof-of-concept of the SCP (smart city platform).
The project uses an arduino uno hardware connected with a luminosity sensor (light  sensor) and leds (like actuators).
>

# How to use

>* In the project directory, run:
>  * ```$ gem install bundle```
>  * ```$ bundle install```
>
> If You are using linux, need use this command to permit USB port access: $sudo usermod -a -G dialout [user_name]

---
Useful links

>* [Project description](https://social.stoa.usp.br/poo2016/projeto/projeto-plataforma-cidades-inteligentes) @ STOA
>* [Group Repository](https://gitlab.com/groups/smart-city-platform)
>* [email list](https://groups.google.com/forum/#!forum/pci-lideres-equipe-de-organizacao-poo-ime-2016)
